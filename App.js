/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import AppContainer from './src/appContainer'
import { Provider } from 'react-redux'
import store from './src/redux/store'
import api from './src/api'
export default class App extends Component {
  constructor(props) {
    super(props)
    api.setStore(store)
  }
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}
