import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, AsyncStorage } from "react-native";
import { createStackNavigator } from "react-navigation";
import Login from "./component/login";
import Drawer from "./component/drawer";
import Home from "./component/home";
import ScanBlueTooth from "./component/sticket/scanBlueTooth";
import Loading from './UI/loading';
import Messagebox from './UI/messagebox'
import ConfirmBox from './UI/confirmBox'
import ScanTicket from './component/sticket/scanTicket'
const AppScreen = name => {
  return createStackNavigator(
    {
      login: { screen: Login },
      home: { screen: Home },
      drawer: { screen: Drawer },
      scanBlueTooth: { screen: ScanBlueTooth },
      scanTicket: { screen: ScanTicket },
      message: { screen: Messagebox },
      loading: { screen: Loading },
      confirmbox: { screen: ConfirmBox },
    },
    {
      headerMode: "none",
      initialRouteName: name,
      mode: "modal",
      cardStyle: { backgroundColor: "transparent", opacity: 1 }
    }
  );
};

export default class AppContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "login"
    };
  }
  componentDidMount() { }

  render() {
    let App = AppScreen(this.state.name);
    return <App />;
  }
}
