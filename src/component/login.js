import React, { Component } from 'react';
import { Toast, Container, Header, Content, Thumbnail, Form, Item, Input, Label, Button, Text, View, } from 'native-base';
import {
    ImageBackground, StatusBar, Image, TouchableOpacity, ScrollView, AsyncStorage,
    Modal,
} from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import api from '../api'
import { Color } from '../themes/color'
import dataService from '../network/dataService';
export default class Login extends Component {
    constructor(props) {
        super(props)
        api.setNavigationState(this.props.navigation)
        this.state = {
            userName: "",
            pass: "",
        }
    }
    componentDidMount() {
        SplashScreen.hide();
        this.checkSession()

    }
    async login() {
        // alert('hehe')
        let { userName, pass } = this.state;
        if (!userName) return api.showMessage('Vui lòng điền tên đăng nhập!')
        if (!pass) return api.showMessage('Vui lòng điền mật khẩu!')
        api.showLoading()
        let rs = await dataService.login(userName, pass);
        api.pop()
        if (!rs.ResultValue) return api.showMessage(rs.ResultMessage)
        rs.UserID = userName;
        api.setUserInfo(rs)
        api.reset(0, "home")
        this.setSecsion(userName)
    }
    async setSecsion(data) {
        try {
            await AsyncStorage.setItem('UserID', data)
        } catch (err) {
            console.log('err set session', err)
        }
    }
    async checkSession() {
        api.showLoading();
        try {
            let data = await AsyncStorage.getItem('UserID');
            console.log(data)
            api.pop();
            if (data) {
                api.setUserInfo({ UserID: data })
                api.reset(0, "home")
            }
        } catch (err) {
            console.log('err get session', err)
        }
    }
    render() {
        return (
            <Container>
                <StatusBar backgroundColor={Color.colorStatusbar} />
                <ImageBackground
                    style={{ flex: 1, alignItems: 'center' }}
                    source={require('../img/background.jpg')}
                >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'
                    >

                        <Image style={{
                            width: api.getRealDeviceWidth() / 3,
                            height: api.getRealDeviceWidth() / 3,
                            marginTop: api.getRealDeviceHeight() / 5,
                            alignSelf: 'center', borderRadius: 15
                        }} source={require('../img/logo.png')} />

                        <View style={{ marginTop: 20, alignItems: 'center' }} >
                            <Item
                                style={{
                                    backgroundColor: 'rgba(248,248,255,0.5)',
                                    borderColor: 'transparent',
                                    width: api.getRealDeviceWidth() - 50,

                                }}
                                rounded>
                                <Input
                                    autoCapitalize='none'
                                    value={this.state.userName}
                                    onChangeText={(userName) => { this.setState({ userName }) }}
                                    selectionColor={'#fff'}
                                    placeholderTextColor='#fff'
                                    style={{
                                        textAlign: 'center',
                                        color: 'yellow'
                                    }} placeholder='Nhập tên đăng nhập'
                                    onSubmitEditing={() => { this.inputPass._root.focus() }}
                                />
                            </Item>
                            <Item
                                style={{
                                    backgroundColor: 'rgba(248,248,255,0.5)',
                                    borderColor: 'transparent',
                                    width: api.getRealDeviceWidth() - 50,
                                    marginTop: 5,

                                }}
                                rounded>
                                <Input
                                    ref={component => this.inputPass = component}
                                    autoCapitalize='none'
                                    secureTextEntry={true}
                                    value={this.state.pass}
                                    onChangeText={(pass) => { this.setState({ pass }) }}
                                    selectionColor={'#fff'}
                                    placeholderTextColor='#fff'
                                    placeholder='Nhập mật khẩu của bạn'
                                    style={{
                                        textAlign: 'center',
                                        color: 'yellow'
                                    }}
                                    onSubmitEditing={() => { this.login() }}

                                />
                            </Item>
                            <Button
                                // onPress={() => { api.navigate("home"); }}
                                onPress={() => { this.login() }}
                                block light style={{ backgroundColor: '#fff', borderRadius: 25, height: 50, marginTop: 15, width: api.getRealDeviceWidth() - 50, alignSelf: 'center' }}>
                                <Text allowFontScaling={false} style={{ color: Color.colorPrimari, fontWeight: 'bold', backgroundColor: 'transparent' }}>Đăng nhập</Text>
                            </Button>
                            {/* <Text allowFontScaling={false} style={{ alignSelf: 'center', marginTop: 5, color: '#fff', backgroundColor: 'transparent', padding: 10, }} >Bạn chưa có tài khoản ? Vui lòng <Text allowFontScaling={false} onPress={() => { this.setState({ mdCallRegister: true }) }} style={{ color: '#fff', fontWeight: 'bold', textDecorationLine: 'underline', backgroundColor: 'transparent' }}>ĐĂNG KÝ</Text> </Text> */}
                        </View>
                    </ScrollView>
                </ImageBackground>
            </Container>
        );
    }
}