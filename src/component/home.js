import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text, Platform,
    FlatList,
    ActivityIndicator,
    WebView, AsyncStorage,
    Modal, StatusBar,
    Button,
    ScrollView,
    DeviceEventEmitter,
    NativeEventEmitter,
    Dimensions,
    ToastAndroid,
    Alert,
    TextInput,
    ImageBackground
} from 'react-native';
import { Icon, Container, Header, Content, Tab, Tabs } from 'native-base'
import LinearGradient from 'react-native-linear-gradient';
import { Color } from '../themes/color';
import api from '../api';
import dataService from '../network/dataService'
import { connect } from 'react-redux'
import ScanBlueTooth from './sticket/scanBlueTooth'
import { BluetoothEscposPrinter, BluetoothManager, BluetoothTscPrinter } from "react-native-bluetooth-escpos-printer";
const { height, width } = Dimensions.get('window');
const dateFormat = require('dateformat');
import { logo } from './sticket/imageBase64'
import KeyboardSpacer from 'react-native-keyboard-spacer';
import moment from 'moment'

class Home extends Component {
    _listeners = [];
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            devices: null,
            pairedDs: [],
            foundDs: [],
            bleOpend: false,
            loading: true,
            boundAddress: '',
            orderNum: ''
        }
    }
    async logOut() {
        try {
            let rs = await dataService.logOff(this.props.UserID);
            if (!rs.ResultData) return api.showMessage(rs.ResultMessage)
            await AsyncStorage.removeItem('UserID');
            api.reset(0, "login");
        } catch (err) {
            console.log('err remove data', err)
        }
    }
    componentDidMount() {
        BluetoothManager.isBluetoothEnabled().then((enabled) => {
            this.setState({
                bleOpend: Boolean(enabled),
                loading: false
            })
        }, (err) => {
            err
        });

        if (Platform.OS === 'ios') {
            let bluetoothManagerEmitter = new NativeEventEmitter(BluetoothManager);
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_DEVICE_ALREADY_PAIRED,
                (rsp) => {
                    this._deviceAlreadPaired(rsp)
                }));
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_DEVICE_FOUND, (rsp) => {
                this._deviceFoundEvent(rsp)
            }));
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_CONNECTION_LOST, () => {
                this.setState({
                    name: '',
                    boundAddress: ''
                });
            }));
        } else if (Platform.OS === 'android') {
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_DEVICE_ALREADY_PAIRED, (rsp) => {
                    this._deviceAlreadPaired(rsp)
                }));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_DEVICE_FOUND, (rsp) => {
                    this._deviceFoundEvent(rsp)
                }));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_CONNECTION_LOST, () => {
                    this.setState({
                        name: '',
                        boundAddress: ''
                    });
                }
            ));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_BLUETOOTH_NOT_SUPPORT, () => {
                    ToastAndroid.show("Device Not Support Bluetooth !", ToastAndroid.LONG);
                }
            ))
        }
    }

    _deviceAlreadPaired(rsp) {
        var ds = null;
        if (typeof (rsp.devices) == 'object') {
            ds = rsp.devices;
        } else {
            try {
                ds = JSON.parse(rsp.devices);
            } catch (e) {
            }
        }
        if (ds && ds.length) {
            let pared = this.state.pairedDs;
            pared = pared.concat(ds || []);
            this.setState({
                pairedDs: pared
            });
        }
    }

    _deviceFoundEvent(rsp) {//alert(JSON.stringify(rsp))
        var r = null;
        try {
            if (typeof (rsp.device) == "object") {
                r = rsp.device;
            } else {
                r = JSON.parse(rsp.device);
            }
        } catch (e) {//alert(e.message);
            //ignore
        }
        //alert('f')
        if (r) {
            let found = this.state.foundDs || [];
            if (found.findIndex) {
                let duplicated = found.findIndex(function (x) {
                    return x.address == r.address
                });
                //CHECK DEPLICATED HERE...
                if (duplicated == -1) {
                    found.push(r);
                    this.setState({
                        foundDs: found
                    });
                }
            }
        }
    }

    _renderRow(rows) {
        let items = [];
        for (let i in rows) {
            let row = rows[i];
            if (row.address) {
                items.push(
                    <TouchableOpacity
                        key={new Date().getTime() + i}
                        style={styles.wtf}
                        onPress={() => {
                            this.setState({ loading: true });
                            BluetoothManager.connect(row.address)
                                .then((s) => {
                                    this.setState({
                                        loading: false,
                                        boundAddress: row.address,
                                        name: row.name || "UNKNOWN"
                                    })
                                }, (e) => {
                                    this.setState({
                                        loading: false
                                    });
                                    Alert.alert(
                                        'Lỗi kết nối',
                                        'Vui lòng kiểm tra máy in, hoặc khởi động lại máy in để làm mới kết nối!',
                                        [
                                            { text: 'Đóng', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                                        ],
                                        { cancelable: false }
                                    )
                                })

                        }}>
                        <Text style={styles.name}>{row.name || "UNKNOWN"}</Text>
                        <Text style={styles.address}>{row.address}</Text>
                    </TouchableOpacity>
                );
            }
        }
        return items;
    }
    async printTicket() {
        if (!this.state.orderNum) return api.showMessage('Vui lòng nhập mã vé')
        try {
            let rs = await dataService.getTicket(this.state.orderNum);
            // let rs = await dataService.getTicket('SO001-00000263');
            if (!rs.length) return api.showMessage('không tìm được thông tin vé')
            var numberPattern = /\d+/g;
            let start = rs[0].VALIDFROM.match(numberPattern)
            let end = rs[0].VALIDTO.match(numberPattern)
            let buy = rs[0].ORDERDATE.match(numberPattern)
            console.log('ggggggg', moment(parseInt(end[0])));
            await BluetoothEscposPrinter.printerInit();
            await BluetoothEscposPrinter.printerLeftSpace(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText("COCOBUS TOUR\r\n\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 2,
                heigthtimes: 2,
                fonttype: 1
            });
            await BluetoothEscposPrinter.printPic(logo, { width: 150, height: 150, left: 200 });
            await BluetoothEscposPrinter.printText("\r\n", {});
            await BluetoothEscposPrinter.printText("\r\n\r\n", {});
            // await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printerLeftSpace(150);
            await BluetoothEscposPrinter.printQRCode(rs[0].QRCODE, 280, BluetoothEscposPrinter.ERROR_CORRECTION.L);
            await BluetoothEscposPrinter.printerLeftSpace(0);
            await BluetoothEscposPrinter.printText("Ma hoa don： " + (rs[0].ORDERNUM) + "\r\n", {});
            await BluetoothEscposPrinter.printText("Ngay mua： " + (moment(parseInt(buy[0])).format('DD-MM-YYYY')) + "\r\n", {});
            await BluetoothEscposPrinter.printText("Nhan vien： " + (rs[0].PUBLISHBY) + "\r\n", {});
            await BluetoothEscposPrinter.printText("Ap dung tu ngay： " + (moment(parseInt(start[0])).format('DD-MM-YYYY')) + "\r\n", {});
            await BluetoothEscposPrinter.printText("Ap dung den ngay：" + (moment(parseInt(end[0])).format('DD-MM-YYYY')) + "\r\n", {});
            await BluetoothEscposPrinter.printText("\r\n\r\n\r\n", {});

            // await BluetoothEscposPrinter.printText("Thoi gian ：" + (dateFormat(moment(parseInt(end[0])), "dd-mm-yyyy")) + "\r\n\r\n", { left: 200 });
            // await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            // await BluetoothEscposPrinter.printBarCode("123456789012", BluetoothEscposPrinter.BARCODETYPE.JAN13, 3, 120, 0, 2);

        } catch (e) {
            alert(e.message || "ERROR");
        }

    }
    toggleBlue() {
        if (this.state.bleOpend) {
            BluetoothManager.disableBluetooth().then(() => {
                this.setState({
                    bleOpend: false,
                    loading: false,
                    foundDs: [],
                    pairedDs: []
                });
            }, (err) => { alert(err) });

        } else {
            BluetoothManager.enableBluetooth().then((r) => {
                var paired = [];
                if (r && r.length > 0) {
                    for (var i = 0; i < r.length; i++) {
                        try {
                            paired.push(JSON.parse(r[i]));
                        } catch (e) {
                            //ignore
                        }
                    }
                }
                this.setState({
                    bleOpend: true,
                    loading: false,
                    pairedDs: paired
                })
            }, (err) => {
                this.setState({
                    loading: false
                })
                alert(err)
            });
        }
    }
    _scan() {
        this.setState({
            loading: true
        })
        BluetoothManager.scanDevices()
            .then((s) => {
                var ss = s;
                var found = ss.found;
                try {
                    found = JSON.parse(found);//@FIX_it: the parse action too weired..
                } catch (e) {
                    //ignore
                }
                var fds = this.state.foundDs;
                if (found && found.length) {
                    fds = found;
                }
                this.setState({
                    foundDs: fds,
                    loading: false
                });
            }, (er) => {
                this.setState({
                    loading: false
                })
                alert('error' + JSON.stringify(er));
            });
    }

    renderScanBlue() {
        if (!this.state.showModal) return null
        return (
            <View style={{ zIndex: 2, width: api.getRealDeviceWidth(), height: api.getRealDeviceHeight(), position: 'absolute' }}>
                <View style={{
                    backgroundColor: 'white', flex: 1, justifyContent: 'center'
                }}>
                    <View style={{ flexDirection: 'row', backgroundColor: Color.colorPrimari, padding: 10 }}>
                        <TouchableOpacity
                            style={{ width: 50, marginLeft: 10 }}
                            onPress={() => this.setState({ showModal: false })}
                        >
                            <Icon type='Ionicons' name='ios-arrow-back' style={{ color: 'white' }} />
                        </TouchableOpacity>
                        <Text style={{ marginLeft: 20, textAlign: 'center', color: 'white', fontWeight: 'bold', fontSize: 16 }}>Kết nối BlueTooth và In vé</Text>

                    </View>
                    <ScrollView
                        style={styles.container}
                        ref={component => this.scroll = component}
                    >
                        <Text style={{ marginLeft: 10, fontWeight: 'bold', marginTop: 10 }}>Mã vé</Text>
                        <TextInput
                            placeholder='Ví dụ: SO0001-000*******'
                            value={this.state.orderNum}
                            onChangeText={text => this.setState({ orderNum: text })}
                            style={{ marginLeft: 8, marginRight: 8 }}
                            onFocus={() => { this.scroll.scrollToEnd() }}
                        />
                        <View style={{ flexDirection: "row", justifyContent: "space-around", paddingVertical: 30 }}>
                            <Button
                                disabled={this.state.loading || !(this.state.bleOpend && this.state.boundAddress.length > 0)}
                                title="IN VÉ" onPress={() => { this.printTicket() }} />
                        </View>
                        <View style={{ flexDirection: 'row', padding: 10, backgroundColor: '#eee', alignItems: 'center' }}>
                            <Text style={{ fontWeight: 'bold' }}>Blutooth: </Text>
                            <TouchableOpacity
                                onPress={() => this.toggleBlue()}
                                style={{ backgroundColor: this.state.bleOpend ? 'green' : 'gray', padding: 8, borderRadius: 5 }}
                            >
                                <Text style={{ color: 'white', fontWeight: 'bold' }}>{this.state.bleOpend ? " Đã bật" : " Đã tắt, chạm để bật"}</Text>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Button disabled={this.state.loading || !this.state.bleOpend} onPress={() => {
                                this._scan();
                            }} title="Quét tìm máy in" />
                        </View>
                        {!this.state.name ?
                            <Text style={{ color: 'red', margin: 5 }}>Vui lòng quyét để tìm và kết nối với máy in !</Text> :
                            <Text style={{ color: 'red', margin: 5, fontWeight: 'bold', fontSize: 17 }}>Đã kết nối với: {this.state.name}</Text>
                        }
                        {/* <Text style={styles.title}>Kết nối tới: {!this.state.name ? 'No Devices' : this.state.name}</Text> */}
                        {this.state.loading ? (<ActivityIndicator animating={true} />) : null}
                        <Text style={styles.title}>Các thiết bị tìm thấy( Chạm để kết nối ):</Text>
                        <View style={{ flex: 1, flexDirection: "column", padding: 8 }}>
                            {
                                this._renderRow(this.state.foundDs)
                            }
                        </View>
                        <Text style={styles.title}>Đang ghép đôi( Chạm để kết nối ):</Text>
                        {this.state.loading ? (<ActivityIndicator animating={true} />) : null}
                        <View style={{ flex: 1, flexDirection: "column", padding: 8 }}>
                            {
                                this._renderRow(this.state.pairedDs)
                            }
                        </View>
                        <KeyboardSpacer />
                    </ScrollView>
                </View>
            </View>
        )
    }
    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <StatusBar backgroundColor={Color.colorStatusbar} />
                {this.renderScanBlue()}
                <View style={{ flexDirection: 'row', justifyContent: 'space-around', minHeight: 65, alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => Alert.alert(
                            'Thoát tài khoàn',
                            'Bạn có muốn thoát tài khoản không?',
                            [
                                {
                                    text: 'Không',
                                    onPress: () => console.log('Cancel Pressed'),
                                    style: 'cancel',
                                },
                                { text: 'Có', onPress: () => { this.logOut() } },
                            ],
                            { cancelable: false },
                        )}
                    >
                        <Icon type='SimpleLineIcons' name='logout' style={{}} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => this.props.navigation.navigate('scanTicket')}
                    // onPress={() => this.setState({ showModal: true })}
                    >
                        <LinearGradient
                            colors={['#C25CB6', '#C22FB1', '#C310AE']}
                            style={styles.btn}
                        >
                            <Icon type='FontAwesome' name='qrcode' style={{ color: '#fff200', marginRight: 10 }} />
                            <Text style={{ fontWeight: 'bold', color: '#fff200' }}>QUÉT MÃ</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        // onPress={() => this.props.navigation.navigate('scanBlueTooth')}
                        onPress={() => this.setState({ showModal: true })}
                    >
                        <LinearGradient
                            colors={['#C25CB6', '#C22FB1', '#C310AE']}
                            style={styles.btn}
                        >
                            <Icon type='FontAwesome' name='print' style={{ color: '#fff200', marginRight: 10 }} />
                            <Text style={{ fontWeight: 'bold', color: '#fff200' }}>In</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
                <ImageBackground source={require('../img/background.jpg')} style={{ flex: 1 }}>
                </ImageBackground>
                <View style={{ position: 'absolute', zIndex: 1, width: width, height: height - 40, top: 65 }}>
                    {this.state.visible ? <ActivityIndicator size='large' color='white' /> : null}
                    <WebView
                        source={{ uri: 'http://bus.cocobay.vn:8080/Login.aspx' }}
                        style={{ backgroundColor: 'rgba(1,1,1,0.5)', }}
                        onLoadStart={() => (this.showSpinner())}
                        onLoad={() => (this.hideSpinner())}
                    />

                </View>

                {/* <WebView source={{ uri: 'http://bus.cocobay.vn:8080/Login.aspx' }} /> */}


            </View>
        );
    }
    showSpinner() {
        console.log('Show Spinner');
        this.setState({ visible: true });
    }

    hideSpinner() {
        console.log('Hide Spinner');
        this.setState({ visible: false });
    }
}
const styles = StyleSheet.create({
    btn: {
        flexDirection: 'row',
        padding: 10,
        borderRadius: 10,
        minWidth: 120,
        maxHeight: 45,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        backgroundColor: '#F5FCFF',
    },

    title: {
        width: width,
        backgroundColor: "#eee",
        color: "#232323",
        paddingLeft: 8,
        paddingVertical: 4,
        textAlign: "left"
    },
    wtf: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: '#ffebee',
        padding: 5,
        marginTop: 5
    },
    name: {
        flex: 1,
        textAlign: "left",
    },
    address: {
        flex: 1,
        textAlign: "right",
        textAlignVertical: 'center'
    }
})
const mapStateToProp = (state) => {
    return {
        UserID: state.userReducer.UserID
    }
}
export default connect(mapStateToProp)(Home)


