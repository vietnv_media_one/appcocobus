import { Body, Button, Card, CardItem, Container, Content, Drawer, Header, Icon, Left, List, ListItem, Right, Title } from 'native-base';
import React, { Component } from 'react';
import { AsyncStorage, Image, RefreshControl, ScrollView, Text, View, Linking } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { connect } from 'react-redux';
import api from '../api';
import { Color } from '../themes/color';
import Home from '../component/home'
export default class MainDrawer extends Component {
    constructor(props) {
        super(props)
        api.setNavigationState(this.props.navigation)
        this.closeDrawer = this.closeDrawer.bind(this)
        this.openDrawer = this.openDrawer.bind(this)
    }
    componentDidMount() {
        SplashScreen.hide()
    }

    closeDrawer = () => {
        this.drawer._root.close()
    };
    openDrawer = () => {
        this.drawer._root.open()
    };

    async _logOut() {
        await AsyncStorage.clear()
        api.reset(0, 'login')
        setTimeout(() => { api.clearStoreUser() }, 1000)
    }

    render() {
        return (
            <Drawer
                ref={(ref) => { this.drawer = ref; }}
                content={
                    <Container>
                        <Header
                            androidStatusBarColor={Color.colorStatusbar}
                            style={{ backgroundColor: Color.colorPrimari }}
                        >
                            <Body>
                                <Title>Menu</Title>
                            </Body>
                        </Header>
                        <Content>
                            <List>
                                <ListItem
                                    onPress={() => {
                                        this.closeDrawer();
                                    }}
                                >
                                    <Text allowFontScaling={false} >Lịch sử quét mã</Text>
                                </ListItem>
                                <ListItem
                                    onPress={() => {
                                        this.closeDrawer();
                                    }}
                                >
                                    <Text allowFontScaling={false} >Lịch sử in </Text>
                                </ListItem>
                                <ListItem onPress={() => { api.showConfirm('Bạn có muốn đăng xuất ?', 'Đăng xuất', () => { api.pop() }, this._logOut.bind(this)) }} >
                                    <Text allowFontScaling={false}>Đăng xuất</Text>
                                </ListItem>
                            </List>
                        </Content>
                        <Text style={{ position: 'absolute', bottom: 5, left: 20, fontSize: 15 }}>Hotline: <Text onPress={() => { Linking.openURL('tel:0886698585') }} style={{ textDecorationLine: 'underline', color: 'green' }}>0886698585</Text></Text>
                    </Container>
                }
                onClose={() => this.closeDrawer()}
            >
                <Container style={{ backgroundColor: 'transparent' }} >
                    <Header
                        androidStatusBarColor={Color.colorStatusbar}
                        style={{ backgroundColor: Color.colorPrimari }}
                    >
                        <Left>
                            <Button
                                onPress={this.openDrawer}
                                transparent>
                                <Icon style={{ fontSize: 35 }} name='menu' />
                            </Button>
                        </Left>
                        <Body>
                            <Title allowFontScaling={false}>Trang chủ</Title>
                        </Body>
                        <Right>

                        </Right>
                    </Header>
                    <Home />
                </Container>
            </Drawer>
        );
    }
}
