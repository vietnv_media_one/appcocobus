import React, { Component } from 'react';
import {
    ActivityIndicator,
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    ScrollView,
    DeviceEventEmitter,
    NativeEventEmitter,
    Switch,
    TouchableOpacity,
    Dimensions,
    ToastAndroid,
    Alert
} from 'react-native';
import { BluetoothEscposPrinter, BluetoothManager, BluetoothTscPrinter } from "react-native-bluetooth-escpos-printer";
var { height, width } = Dimensions.get('window');
var dateFormat = require('dateformat');
import { logo } from './imageBase64'
export default class ScanBueTooth extends Component {


    _listeners = [];

    constructor() {
        super();
        this.state = {
            devices: null,
            pairedDs: [],
            foundDs: [],
            bleOpend: false,
            loading: true,
            boundAddress: '',
        }
    }

    componentDidMount() {//alert(BluetoothManager)
        BluetoothManager.isBluetoothEnabled().then((enabled) => {
            this.setState({
                bleOpend: Boolean(enabled),
                loading: false
            })
        }, (err) => {
            err
        });

        if (Platform.OS === 'ios') {
            let bluetoothManagerEmitter = new NativeEventEmitter(BluetoothManager);
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_DEVICE_ALREADY_PAIRED,
                (rsp) => {
                    this._deviceAlreadPaired(rsp)
                }));
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_DEVICE_FOUND, (rsp) => {
                this._deviceFoundEvent(rsp)
            }));
            this._listeners.push(bluetoothManagerEmitter.addListener(BluetoothManager.EVENT_CONNECTION_LOST, () => {
                this.setState({
                    name: '',
                    boundAddress: ''
                });
            }));
        } else if (Platform.OS === 'android') {
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_DEVICE_ALREADY_PAIRED, (rsp) => {
                    this._deviceAlreadPaired(rsp)
                }));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_DEVICE_FOUND, (rsp) => {
                    this._deviceFoundEvent(rsp)
                }));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_CONNECTION_LOST, () => {
                    this.setState({
                        name: '',
                        boundAddress: ''
                    });
                }
            ));
            this._listeners.push(DeviceEventEmitter.addListener(
                BluetoothManager.EVENT_BLUETOOTH_NOT_SUPPORT, () => {
                    ToastAndroid.show("Device Not Support Bluetooth !", ToastAndroid.LONG);
                }
            ))
        }
    }

    _deviceAlreadPaired(rsp) {
        var ds = null;
        if (typeof (rsp.devices) == 'object') {
            ds = rsp.devices;
        } else {
            try {
                ds = JSON.parse(rsp.devices);
            } catch (e) {
            }
        }
        if (ds && ds.length) {
            let pared = this.state.pairedDs;
            pared = pared.concat(ds || []);
            this.setState({
                pairedDs: pared
            });
        }
    }

    _deviceFoundEvent(rsp) {//alert(JSON.stringify(rsp))
        var r = null;
        try {
            if (typeof (rsp.device) == "object") {
                r = rsp.device;
            } else {
                r = JSON.parse(rsp.device);
            }
        } catch (e) {//alert(e.message);
            //ignore
        }
        //alert('f')
        if (r) {
            let found = this.state.foundDs || [];
            if (found.findIndex) {
                let duplicated = found.findIndex(function (x) {
                    return x.address == r.address
                });
                //CHECK DEPLICATED HERE...
                if (duplicated == -1) {
                    found.push(r);
                    this.setState({
                        foundDs: found
                    });
                }
            }
        }
    }

    _renderRow(rows) {
        let items = [];
        for (let i in rows) {
            let row = rows[i];
            if (row.address) {
                items.push(
                    <TouchableOpacity
                        key={new Date().getTime() + i}
                        style={styles.wtf}
                        onPress={() => {
                            this.setState({ loading: true });
                            BluetoothManager.connect(row.address)
                                .then((s) => {
                                    this.setState({
                                        loading: false,
                                        boundAddress: row.address,
                                        name: row.name || "UNKNOWN"
                                    })
                                }, (e) => {
                                    this.setState({
                                        loading: false
                                    });
                                    Alert.alert(
                                        'Lỗi kết nối',
                                        'Vui lòng kiểm tra thiết bị, hoặc khởi động lại thiết bị để làm mới kết nối!',
                                        [
                                            { text: 'Đóng', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                                        ],
                                        { cancelable: false }
                                    )
                                })

                        }}>
                        <Text style={styles.name}>{row.name || "UNKNOWN"}</Text>
                        <Text style={styles.address}>{row.address}</Text>
                    </TouchableOpacity>
                );
            }
        }
        return items;
    }
    async printTicket() {
        try {
            await BluetoothEscposPrinter.printerInit();
            await BluetoothEscposPrinter.printerLeftSpace(0);
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.setBlob(0);
            await BluetoothEscposPrinter.printText("COCOBUS TOUR\r\n\r\n", {
                encoding: 'GBK',
                codepage: 0,
                widthtimes: 2,
                heigthtimes: 2,
                fonttype: 1
            });
            await BluetoothEscposPrinter.printPic(logo, { width: 150, height: 150, left: 200 });
            await BluetoothEscposPrinter.printText("\r\n", {});
            await BluetoothEscposPrinter.printText("Ten nhan vien：Vu Dinh Thanh\r\n", {});
            await BluetoothEscposPrinter.printText("Ma nhan vien：HTCO01\r\n", {});
            await BluetoothEscposPrinter.printText("Ma giao dich：COCO01\r\n", {});
            await BluetoothEscposPrinter.printText("Thoi gian：" + (dateFormat(new Date(), "yyyy-mm-dd h:MM:ss")) + "\r\n\r\n", {});
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printBarCode("123456789012", BluetoothEscposPrinter.BARCODETYPE.JAN13, 3, 120, 0, 2);
            await BluetoothEscposPrinter.printText("\r\n\r\n", {});
            await BluetoothEscposPrinter.printerAlign(BluetoothEscposPrinter.ALIGN.CENTER);
            await BluetoothEscposPrinter.printQRCode("cocobay", 280, BluetoothEscposPrinter.ERROR_CORRECTION.L);
            await BluetoothEscposPrinter.printText("\r\n\r\n", {});
        } catch (e) {
            alert(e.message || "ERROR");
        }

    }
    toggleBlue() {
        if (this.state.bleOpend) {
            BluetoothManager.disableBluetooth().then(() => {
                this.setState({
                    bleOpend: false,
                    loading: false,
                    foundDs: [],
                    pairedDs: []
                });
            }, (err) => { alert(err) });

        } else {
            BluetoothManager.enableBluetooth().then((r) => {
                var paired = [];
                if (r && r.length > 0) {
                    for (var i = 0; i < r.length; i++) {
                        try {
                            paired.push(JSON.parse(r[i]));
                        } catch (e) {
                            //ignore
                        }
                    }
                }
                this.setState({
                    bleOpend: true,
                    loading: false,
                    pairedDs: paired
                })
            }, (err) => {
                this.setState({
                    loading: false
                })
                alert(err)
            });
        }
    }
    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={{ flexDirection: 'row', padding: 10, backgroundColor: '#eee', alignItems: 'center' }}>
                    <Text style={{ fontWeight: 'bold' }}>Blutooth: </Text>
                    <TouchableOpacity
                        onPress={() => this.toggleBlue()}
                        style={{ backgroundColor: this.state.bleOpend ? 'green' : 'gray', padding: 8, borderRadius: 5 }}
                    >
                        <Text style={{ color: 'white', fontWeight: 'bold' }}>{this.state.bleOpend ? " Đã bật" : " Đã tắt, chạm để bật"}</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <Button disabled={this.state.loading || !this.state.bleOpend} onPress={() => {
                        this._scan();
                    }} title="Quét tìm máy in" />
                </View>
                {!this.state.name ?
                    <Text style={{ color: 'red', margin: 5 }}>Vui lòng quyét để tìm và kết nối với máy in !</Text> :
                    <Text style={{ color: 'red', margin: 5, fontWeight: 'bold', fontSize: 17 }}>Đã kết nối với: {this.state.name}</Text>
                }
                {/* <Text style={styles.title}>Kết nối tới: {!this.state.name ? 'No Devices' : this.state.name}</Text> */}
                {this.state.loading ? (<ActivityIndicator animating={true} />) : null}
                <Text style={styles.title}>Các thiết bị tìm thấy( Chạm để kết nối ):</Text>
                <View style={{ flex: 1, flexDirection: "column", padding: 8 }}>
                    {
                        this._renderRow(this.state.foundDs)
                    }
                </View>
                <Text style={styles.title}>Đang ghép đôi( Chạm để kết nối ):</Text>
                {this.state.loading ? (<ActivityIndicator animating={true} />) : null}
                <View style={{ flex: 1, flexDirection: "column", padding: 8 }}>
                    {
                        this._renderRow(this.state.pairedDs)
                    }
                </View>

                <View style={{ flexDirection: "row", justifyContent: "space-around", paddingVertical: 30 }}>
                    <Button
                        disabled={this.state.loading || !(this.state.bleOpend && this.state.boundAddress.length > 0)}
                        title="IN VÉ" onPress={() => { this.printTicket() }} />
                </View>
            </ScrollView>
        );
    }
    _scan() {
        this.setState({
            loading: true
        })
        BluetoothManager.scanDevices()
            .then((s) => {
                var ss = s;
                var found = ss.found;
                try {
                    found = JSON.parse(found);//@FIX_it: the parse action too weired..
                } catch (e) {
                    //ignore
                }
                var fds = this.state.foundDs;
                if (found && found.length) {
                    fds = found;
                }
                this.setState({
                    foundDs: fds,
                    loading: false
                });
            }, (er) => {
                this.setState({
                    loading: false
                })
                alert('error' + JSON.stringify(er));
            });
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },

    title: {
        width: width,
        backgroundColor: "#eee",
        color: "#232323",
        paddingLeft: 8,
        paddingVertical: 4,
        textAlign: "left"
    },
    wtf: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: '#ffebee',
        padding: 5,
        marginTop: 5
    },
    name: {
        flex: 1,
        textAlign: "left",
    },
    address: {
        flex: 1,
        textAlign: "right",
        textAlignVertical: 'center'
    }
});