import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    View, Platform,
    PermissionsAndroid
} from 'react-native';
import { Button, } from 'native-base';
import api from '../../api';
import { RNCamera } from 'react-native-camera';
import { Color } from '../../themes/color';
import dataService from '../../network/dataService'
import Permissions from 'react-native-permissions'
import moment from 'moment'

export default class BarCodrScaner extends Component {
    constructor() {
        super();
        this.state = {
            permission: Platform.OS == 'ios' ? true : false
        }
    }
    componentDidMount() {
        this.checkPermission()
    }
    async checkPermission() {
        if (Platform.OS === 'android' && Platform.Version >= 23) {
            let rs = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA);
            if (rs) {
                this.setState({ permission: true });
            } else {
                let userApprove = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA);
                if (userApprove) {
                    this.setState({ permission: true });
                } else {
                    // this.props.navigation.pop();
                }
            }
        } else {
            let response = await Permissions.checkMultiple(['camera', 'photo']);
            if (response.camera == 'authorized' || response.camera == 'undetermined') {
                this.setState({ permission: true });
            } else {
                Alert.alert(
                    i18.t('titleMsgLogin'),
                    i18.t('msgPermissionsCMR'),
                    [{ text: i18.t('Cancel'), onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                    { text: i18.t('gotoSetting'), onPress: () => { Permissions.openSettings() } },
                    ],
                    { cancelable: false }
                )
            }
        }
    }
    render() {
        if (!this.state.permission) return (<View></View>)
        return (
            <RNCamera
                ref={ref => {
                    this.camera = ref;
                }}
                onBarCodeRead={this._qrCodeReceive.bind(this)}
                defaultOnFocusComponent={true}
                style={styles.preview}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.off}
                barCodeTypes={[RNCamera.Constants.BarCodeType.code128, RNCamera.Constants.BarCodeType.qr]}
                permissionDialogTitle={'vui lòng cho phép camera'}
                permissionDialogMessage={'ứng dụng cần cấp quyền sử dụng camera'}
                captureAudio={false}
            >
                <Text style={styles.title}>
                    QUYÉT MÃ VÉ
                </Text>
                <View
                    style={{
                        width: Dimensions.get('screen').width * 0.75,
                        height: Dimensions.get('screen').height * 0.35,
                        borderRadius: 15, borderWidth: 1,
                        marginTop: 25,
                        borderColor: 'white'
                    }}
                >
                </View>
                <Button
                    style={styles.bt}
                    onPress={() => this.props.navigation.pop()}>
                    <Text style={{ color: 'white' }}>Đóng</Text>
                </Button>
            </RNCamera>
        );
    }
    isReceip = false
    async _qrCodeReceive(code) {
        console.log('code scan camera', code);
        console.log('status', this.isReceip);
        if (!this.isReceip) {
            this.isReceip = true
            api.showLoading()
            let rs = await dataService.ticketActive(code.data)
            api.pop()
            if (!rs.length) {
                setTimeout(() => this.isReceip = false, 1500)
                return api.showMessage('Mã vé không đúng, Vui lòng kiểm tra lại')
            }
            var numberPattern = /\d+/g;
            let start = rs[0].VALIDFROM.match(numberPattern)
            let end = rs[0].VALIDTO.match(numberPattern)
            console.log(rs[0].VALIDFROM, moment(1547456433930).format('DD-MM-YYYY'))
            api.showMessage(
                "Thông tin vé: \n" +
                "Mã vé: " + rs[0].ORDERNUM + "\n" +
                "Mã Qr: " + code.data + "\n" +
                "Áp dụng từ ngày: " + moment(parseInt(start[0])).format('DD-MM-YYYY') + '\n' +
                "Áp dụng đến ngày: " + moment(parseInt(end[0])).format('DD-MM-YYYY')
            )
            setTimeout(() => this.isReceip = false, 1500)
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    preview: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        color: 'white',
        fontWeight: 'bold',
        width: Dimensions.get('screen').width,
        height: 50,
        textAlignVertical: 'center',
        lineHeight: 50,
        backgroundColor: 'rgba(1,1,1,0.5)',
        textAlign: 'center',
        position: 'absolute',
        top: 80

    },
    bt: {
        borderRadius: 20,
        paddingRight: 30,
        paddingLeft: 30,
        backgroundColor: Color.colorPrimari,
        alignSelf: 'center',
        marginTop: 25,
        position: 'absolute',
        bottom: 80,
    }
});