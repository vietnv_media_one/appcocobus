import { NavigationActions, StackActions } from 'react-navigation';
import { Dimensions } from 'react-native'
let store = null
let navigationState = null
var deviceWidth = Dimensions.get('screen').width
var deviceHeight = Dimensions.get('screen').height
const api = {
    setStore: (newStore) => {
        store = newStore
    },
    getStore: () => {
        return store
    },
    setNavigationState: (newNav) => {
        navigationState = newNav
    },
    setUserInfo: (user) => {
        store.dispatch({ type: 'SET_USER_INFO', user })
    },
    navigate: (routeName, params, action) => {
        navigationState.dispatch(
            NavigationActions.navigate({
                routeName,
                params,
                action: action,
            })
        )
    },
    showLoading: () => {
        navigationState.dispatch(
            NavigationActions.navigate({
                routeName: 'loading',
            })
        )
    },
    showMessage: (content, title) => {
        navigationState.dispatch(
            NavigationActions.navigate({
                routeName: 'message',
                params: { content, title }
            })
        )
    },
    showConfirm: (content, title, btnHuy, btnOK, titlebntHuy, titlebntOK) => {
        navigationState.dispatch(
            NavigationActions.navigate({
                routeName: 'confirmbox',
                params: { content, title, btnOK, btnHuy, titlebntHuy, titlebntOK }
            })
        )
    },
    reset: (index, route) => {
        navigationState.dispatch(
            StackActions.reset({
                index: index,
                actions: [NavigationActions.navigate({ routeName: route })],
            })
        )
    },
    pop: () => {
        navigationState.pop()
    },
    replace: () => {
        navigationState.dispatch(
            NavigationActions.replace({
                key,
                newKey,
                routeName,
                params,
                action
            })
        )
    },
    getRealDeviceHeight: () => {
        return deviceHeight
    },
    getRealDeviceWidth: () => {
        return deviceWidth
    },
}

export default api;