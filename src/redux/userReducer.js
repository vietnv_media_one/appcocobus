let initialState = {
}
const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_USER_INFO':
            return Object.assign({}, state, action.user)
        default:
            return state
    }
}
export default userReducer