import { createStore, applyMiddleware } from 'redux'
import rootReducer from './rootReducer'
import logger from 'redux-logger'
// const configStore = () => {
//     const store = createStore(rootReducer, applyMiddleware(logger))
//     return store
// }
const configStore = createStore(rootReducer, applyMiddleware(logger))

export default configStore