import React, { Component } from 'react';
import { View, TouchableOpacity, Animated, Button } from 'react-native'
import { Container, CardItem, Body, Card, Text, Content, Right, } from 'native-base';
import api from '../api';
import { Color } from '../themes/color';
export default class Messagebox extends Component {

    constructor(props) {
        super(props)
        this.state = {
            marginAni: new Animated.Value(-70)
        }
    }


    render() {
        return (
            <Container style={{ backgroundColor: 'rgba(1,1,1 ,0.5)', justifyContent: 'center', alignItems: 'center' }} >
                <View style={{ backgroundColor: 'white', padding: 10, borderRadius: 10, overflow: 'hidden', width: '95%' }} >
                    <Text style={{
                        textAlign: 'center', fontWeight: 'bold', color: Color.colorPrimari,
                        borderBottomWidth: 1, marginBottom: 10, paddingBottom: 5,
                        borderColor: '#CCCCCC'
                    }}>Thông báo</Text>
                    <Text>{this.props.navigation.state.params.content + ''}</Text>
                    <TouchableOpacity
                        onPress={() => { api.pop() }}
                        activeOpacity={1}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 15,
                            backgroundColor: 'rgba(158,158,158 ,1)',
                            width: 100, padding: 5, alignSelf: 'center', borderRadius: 5,

                        }}>
                        <Text style={{ color: 'white', fontWeight: 'bold' }}> ĐÓNG </Text>
                    </TouchableOpacity>
                </View>
            </Container>
        );
    }
}