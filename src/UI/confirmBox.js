import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label, Button, Spinner } from 'native-base';
import api from '../api';
import { Color } from '../themes/color';
export default class ConfirmBox extends Component {
    render() {
        let { titlebntHuy, titlebntOK, content, title } = this.props.navigation.state.params
        return (
            <Container
                style={{
                    flex: 1,
                    backgroundColor: 'rgba(0, 0, 0, 0.7)',
                    justifyContent: 'center',
                    alignItems: 'center',

                }}>
                <View style={{ width: '80%', backgroundColor: 'white', borderRadius: 10, padding: 15 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1, borderColor: '#DDDDDD', paddingBottom: 5 }}>
                        <Text style={{ fontWeight: '400', fontSize: 16 }}>{title}</Text>
                    </View>
                    <View style={{ marginTop: 8 }}>
                        <Text style={{ fontSize: 15, alignSelf: 'center', lineHeight: 23 }}>
                            {content}
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'center' }}>
                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => { this.props.navigation.state.params.btnHuy(); api.pop() }}
                            style={{ backgroundColor: '#DDDDDD', height: 35, padding: 8, justifyContent: 'center', alignItems: 'center', borderRadius: 5, minWidth: 80 }}
                        >
                            <Text >{titlebntHuy ? titlebntHuy : 'Từ chối'}</Text>
                        </TouchableOpacity>
                        <Text style={{ width: 25 }} />
                        <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => { this.props.navigation.state.params.btnOK(); api.pop() }}
                            style={{ backgroundColor: Color.colorPrimari, padding: 8, height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 5, minWidth: 80 }}
                        >
                            <Text style={{ color: 'white' }}>{titlebntOK ? titlebntOK : 'Đồng ý'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Container>
        );
    }
}