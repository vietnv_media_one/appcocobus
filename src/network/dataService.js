import request from './request.js';
import config from './config.js';
import cache from '../api/cache'
var dataService = {
    login(UserID, Password) {
        let router = 'LogOn?'
        let data = {
            SecureID: '115EF3998',
            UserID,
            Password,
            StoreID: '001',
            TerminalID: '001',
            ShiftID: '1',
            LocationID: '001'
        }
        return request.get(router, data)
    },
    logOff(UserID) {
        let router = 'logOff?'
        let data = {
            SecureID: '115EF3998',
            UserID,
            SessionID: '1'
        }
        return request.get(router, data)
    },
    checkTicket(QrCode) {
        let router = 'TicketScan?'
        let data = {
            SecureID: '115EF3998',
            QrCode
        }
        return request.get(router, data)
    },
    ticketActive(QrCode) {
        let router = 'TicketActive?'
        let data = {
            SecureID: '115EF3998',
            QrCode,
            StaffId: '',
            Latitude: '',
            Longitude: ''
        }
        return request.get(router, data)
    },

    newTicket(OrderNum, LineNum, StaffId) {
        let router = 'TicketNew?'
        let data = {
            SecureID: '115EF3998',
            OrderNum, LineNum, StaffId
        }
        return request.get(router, data)
    },
    getTicket(OrderNum) {
        let router = 'TicketGet?'
        let data = {
            SecureID: '115EF3998',
            OrderNum,
            LineNum: "10000"
        }
        return request.get(router, data)
    },


}
export default dataService;