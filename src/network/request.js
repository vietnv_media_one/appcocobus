import config from './config.js'
import api from '../api';
import qs from 'qs'

let request = {
    get: async (router, data) => {
        console.log('-------------------------GET------------------------ \n', config.HOST + router + qs.stringify(data));
        let rs = await fetch(config.HOST + router + qs.stringify(data));
        console.log('-------rs-----', rs)
        let rsJson = await rs.json()
        console.log('-------------------------RESPONSE------------------------ \n', rsJson);
        if (typeof rsJson == 'object') return rsJson
        return JSON.parse(rsJson);
    },

}

export default request;